/* Activity - Quiz

1. How do you create arrays in JS?
	
	Ans. We can create an array thru the use of assignment operator and array literal.

2. How do you access the first character of an array?
	
	Ans. We can access the first character of an array by first using the .toString() method and then by using index [0]

3. How do you access the last character of an array?
	
	Ans. We can access the last character of an array by first using the .toString() method and then by using the index [string.length-1]
	

4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
	
	Ans. indexOf() method

5. What array method loops over all elements of an array, performing a user-defined function on each iteration?
	
	Ans. forEach() method

6. What array method creates a new array with elements obtained from a user-defined function?
	
	Ans. map() method


7. What array method checks if all its elements satisfy a given condition?
	
	Ans. every() method

8. What array method checks if at least one of its elements satisfies a given condition?
	
	Ans. some() method

9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
	
	Ans. False

10.True or False: array.slice() copies elements from original array and returns them as a new array.
	
	Ans. True

*/

/*Activity - Function Coding*/
let students = ["John", "Joe", "Jane", "Jessie"];

// 1.

function addToEnd(arr, str) {

	// Check if str is not a string
	if (typeof str !== "string") {
	return "error - can only add strings to an array";

	// Return the updated arr
	} else {
		arr.push(str);
		return arr;
	}
};


// 2.

function addToStart(arr, str) {

	// Check if str is not a string
	if (typeof str !== "string") {
	return "error - can only add strings to an array";

	// Return the updated arr
	} else {
		arr.unshift(str);
		return arr;
	}
};


// 3.

function elementChecker(arr, str) {

	// Check if array is empty
  	if (arr.length === 0) {
    	return "error - passed in array is empty";
  	}
  
  	// Check if at least one element has the value of str
  	for (let i = 0; i < arr.length; i++) {
    	if (arr[i] === str) {
     	return true;
    	}
  	}
  
  	return false;
};


// 4.

function checkAllStringsEnding(arr, char) {

	// Check if array is empty
 	if (arr.length === 0) {
    	return "error - array must NOT be empty";
  	}

  	// Check if at least one element is NOT a string
  	if (arr.some((element) => typeof element !== "string")) {
    	return "error - all array elements must be strings";
  	}

  	// Check if 2nd argument is not a string
  	if (typeof char !== "string") {
   	 	return "error - 2nd argument must be of data type string";
  	}

  	// Check if 2nd argument is more than 1 character
  	if (char.length > 1) {
   		return "error - 2nd argument must be a single character";
 	}

 	// Check if every element ends with char
  	return arr.every((element) => element.endsWith(char));
};


// 5.

function stringLengthSorter(arr) {
	  	
  	// Check if all elements are strings
  	for (let i = 0; i < arr.length; i++) {
    	if (typeof arr[i] !== 'string') {
      		return 'error - all array elements must be strings';
    	}
  	}
  
  	// Sort the array based on string length
  	arr.sort((a, b) => a.length - b.length);

  		return arr;
};


// 6.

function startsWithCounter(arr, char) {

  	// Check if array is empty
  	if (arr.length === 0) {
    	return "error - array must NOT be empty";
  	}

  	// Check if at least one element is NOT a string
  	if (arr.some((elem) => typeof elem !== "string")) {
    	return "error - all array elements must be strings";
  	}

  	// Check if 2nd argument is not a string
  	if (typeof char !== "string") {
    	return "error - 2nd argument must be of data type string";
  	}

  	// Check if 2nd argument is more than 1 character
  	if (char.length > 1) {
    	return "error - 2nd argument must be a single character";
  	}

  	// Count elements starting with char argument
  	const count = arr.filter((elem) =>
    	elem.toLowerCase().startsWith(char.toLowerCase())
  		).length;

  	return count;
};


// 7.

function likeFinder(arr, string) {

  	// Check if array is empty
  	if (arr.length === 0) {
    	return "error - array must NOT be empty";
  	}

  	// Check if at least one array element is NOT a string
  	for (let i = 0; i < arr.length; i++) {
    	if (typeof arr[i] !== "string") {
      		return "error - all array elements must be strings";
    	}
  	}

  	// Check if 2nd argument is not a string
  	if (typeof string !== "string") {
    	return "error - 2nd argument must be of data type string";
  	}

  	// Create a new array to hold matching elements
  	let matchingElements = [];

  	// Loop through array and add matching elements to new array
  	for (let i = 0; i < arr.length; i++) {
    
    // Add all elements that contain string(case-insensitive)
    	if (arr[i].toLowerCase().includes(string.toLowerCase())) {
      	matchingElements.push(arr[i]);
    	}
  	}

  	// Return the new array of matching elements
  	return matchingElements;
};


// 8.

function randomPicker(arr) {

  	// Check if the array is empty
  	if (arr.length === 0) {
    	return "error - array must NOT be empty";
  	}

  	// Create a random index based on the length of the array
  	const randomIndex = Math.floor(Math.random() * arr.length);

  	// Return the element of the created random index
  	return arr[randomIndex];
};


